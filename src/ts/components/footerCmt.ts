import { Component } from '@angular/core';
declare var $:any;

@Component({
    selector: 'footer-cmt',
    templateUrl: '../../tpl/footerCmt.html'
})
export class FooterCmt {

    toTop(){

        $('body,html').animate({scrollTop:0},800);

    }
}