import { Component } from '@angular/core';
declare var $:any;

@Component({
    selector: 'header-menu-cmt',
    templateUrl: '../../tpl/headerMenuCmt.html'
})
export class HeaderMenuCmt {
    menuItems: object[] = [
        {url: '#sliderAnchor', name: 'To slider'},
        {url: '#formAnchor', name: 'To form'},
        {url: '#mapAnchor', name: 'To map'},
    ];

    scrollTo(e:any){
        e.preventDefault();
        let id  = e.target.hash,
            top = $(id).offset().top;
        $('body,html').stop().animate({scrollTop: top}, 1500);
    }
}