import { Component, OnInit, AfterViewInit } from '@angular/core';
import 'slick-carousel';
import AppHelper from '../helpers/appHelper';
import { NgModel} from '@angular/forms';
declare var $:any;

@Component({
    selector: 'main-cmt',
    templateUrl: '../../tpl/mainCmt.html'
})
export class MainCmt implements OnInit, AfterViewInit{

    sliderItems: object[] = [
        {img: '/images/content/slider/slide-c1.jpg', text: 'Some text 1'},
        {img: '/images/content/slider/slide-c2.jpg', text: 'Some text 2'},
        {img: '/images/content/slider/slide-c3.jpg', text: 'Some text 3'},
        {img: '/images/content/slider/slide-c4.jpg', text: 'Some text 4'},
        {img: '/images/content/slider/slide-c5.jpg', text: 'Some text 5'},
    ];

    ngOnInit(){

    }

    ngAfterViewInit(){

        let appHelper:AppHelper = new AppHelper();

        $('.slider-section__slider-wrap').slick({
            autoplay: true,
            slidesToShow: 4,
            arrows: false
        });

        appHelper.initgmap(50.442822, 30.529917, 'gmap');

    }

    onSubmit(myForm: NgModel){
        console.log(myForm);
    }
}