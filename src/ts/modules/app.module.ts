import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { AppComponent }   from '../components/appRoot';
import { HeaderCmt }   from '../components/headerCmt';
import { FooterCmt }   from '../components/footerCmt';
import { MainCmt }   from '../components/mainCmt';
import { HeaderMenuCmt }   from '../components/headerMenuCmt';

@NgModule({
    imports:      [ BrowserModule, FormsModule ],
    declarations: [ AppComponent, HeaderCmt, FooterCmt, MainCmt, HeaderMenuCmt ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }
